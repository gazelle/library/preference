package net.ihe.gazelle.preferences;

import java.util.Date;

public interface PreferenceProvider extends Comparable<PreferenceProvider> {

    Object getObject(Object key);

    String getString(String key);

    Integer getInteger(String key);

    Boolean getBoolean(String key);

    Date getDate(String key);

    void setObject(Object key, Object value);

    void setString(String key, String value);

    void setInteger(String key, Integer value);

    void setBoolean(String key, Boolean value);

    void setDate(String key, Date value);

    Integer getWeight();
}
