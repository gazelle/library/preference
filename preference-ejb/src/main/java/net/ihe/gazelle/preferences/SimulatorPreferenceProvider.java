package net.ihe.gazelle.preferences;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;

/**
 * <p>SimulatorPreferenceProvider class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Named("simulatorPreferenceProvider")
@ApplicationScoped
//FIXME Implement all methods/ Deprecate in favor of DAO ?
public class SimulatorPreferenceProvider implements PreferenceProvider {

	@Inject
	private BeanManager beanManager;

	@Inject
	private ApplicationConfigurationDAO applicationConfigurationDAO;

	/** {@inheritDoc} */
	@Override
	public int compareTo(PreferenceProvider o) {
		return getWeight().compareTo(o.getWeight());
	}

    /** {@inheritDoc} */
	@Override
	public Boolean getBoolean(String key) {
		String prefAsString = getString(key);
		if (prefAsString != null && !prefAsString.isEmpty()){
			return Boolean.valueOf(prefAsString);
		}else{
			return null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public Date getDate(String arg0) {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public Integer getInteger(String key) {
		String prefAsString = getString(key);
		if (prefAsString != null && !prefAsString.isEmpty()){
			try{
				return Integer.decode(prefAsString);
			}catch(NumberFormatException e){
				return null;
			}
		}else{
			return null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public Object getObject(Object arg0) {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public String getString(String key) {
	    if (applicationConfigurationDAO != null){
            return applicationConfigurationDAO.getValue(key);
        } else {
	        return null;
        }
	}

	/** {@inheritDoc} */
	@Override
	public Integer getWeight() {

		if (beanManager.getContext(ApplicationScoped.class).isActive()) {
			return -100;
		} else {
			return 100;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void setBoolean(String arg0, Boolean arg1) {
		
	}

	/** {@inheritDoc} */
	@Override
	public void setDate(String arg0, Date arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setInteger(String arg0, Integer arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setObject(Object arg0, Object arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setString(String arg0, String arg1) {
	}

}
