package net.ihe.gazelle.preferences;

import javax.enterprise.inject.spi.CDI;
import java.util.Date;


public class PreferenceService {

	private PreferenceService() {
		super();
	}

	private static PreferenceProvider getProvider() {
		return CDI.current().select(PreferenceProvider.class).get();
	}

	public static String getString(String key) {
		PreferenceProvider pp = getProvider();
		String preference = pp.getString(key);
		CDI.current().destroy(pp);
		return preference;
	}

	public static Integer getInteger(String key) {
		PreferenceProvider pp = getProvider();
		Integer preference = pp.getInteger(key);
		CDI.current().destroy(pp);
		return preference;
	}

	public static Object getObject(String key) {
		PreferenceProvider pp = getProvider();
		Object preference = pp.getObject(key);
		CDI.current().destroy(pp);
		return preference;
	}

	public static Boolean getBoolean(String key) {
		PreferenceProvider pp = getProvider();
		Boolean preference = pp.getBoolean(key);
		CDI.current().destroy(pp);
		return preference;
	}

	public static void setObject(Object key, Object value) {
		PreferenceProvider pp = getProvider();
		pp.setObject(key, value);
		CDI.current().destroy(pp);
	}

	public static void setString(String key, String value) {
		PreferenceProvider pp = getProvider();
		pp.setString(key, value);
		CDI.current().destroy(pp);
	}

	public static void setInteger(String key, Integer value) {
		PreferenceProvider pp = getProvider();
		pp.setInteger(key, value);
		CDI.current().destroy(pp);
	}

	public static void setBoolean(String key, Boolean value) {
		PreferenceProvider pp = getProvider();
		pp.setBoolean(key, value);
		CDI.current().destroy(pp);
	}

	public static void setDate(String key, Date value) {
		PreferenceProvider pp = getProvider();
		pp.setDate(key, value);
		CDI.current().destroy(pp);
	}

}
