package net.ihe.gazelle.preferences;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

@Named("cspPoliciesPreferences")
public class CSPPoliciesPreferencesImpl implements CSPPoliciesPreferences {

    @Inject
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    /** {@inheritDoc} */
    @Override
    public boolean isContentPolicyActivated() {
        return applicationConfigurationDAO.getBooleanValue(PreferencesKey.SECURITY_POLICIES.getFriendlyName());
    }

    /** {@inheritDoc} */
    @Override
    public Map<String, String> getHttpSecurityPolicies() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(PreferencesKey.SECURITY_POLICIES.getFriendlyName(),
                String.valueOf(applicationConfigurationDAO.getBooleanValue(PreferencesKey.SECURITY_POLICIES.getFriendlyName())));
        headers.put(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName(),
                applicationConfigurationDAO.getValue(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName()));
        headers.put(PreferencesKey.CACHE_CONTROL.getFriendlyName(),
                applicationConfigurationDAO.getValue(PreferencesKey.CACHE_CONTROL.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName(),
                applicationConfigurationDAO.getValue(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName(),
                applicationConfigurationDAO.getValue(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName()));
        headers.put(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName(),
                applicationConfigurationDAO.getValue(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_CHROME.getFriendlyName(),
                applicationConfigurationDAO.getValue(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY_CHROME.getFriendlyName(),
                applicationConfigurationDAO.getValue(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName()));
        return headers;
    }

    /** {@inheritDoc} */
    @Override
    public boolean getSqlInjectionFilterSwitch() {
        return applicationConfigurationDAO.getBooleanValue(PreferencesKey.SQL_INJECTION_FILTER_SWITCH.getFriendlyName());
    }

}
