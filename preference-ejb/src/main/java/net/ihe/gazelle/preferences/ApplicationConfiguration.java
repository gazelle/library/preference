package net.ihe.gazelle.preferences;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Abderrazek Boufahja INRIA Rennes IHE development Project
 */
@Entity
@Table(name = "app_configuration", schema = "public", uniqueConstraints = {
        @UniqueConstraint(name = "uk_app_configuration_variable", columnNames = {"variable"})})
@SequenceGenerator(name = "app_configuration_sequence", sequenceName = "app_configuration_id_seq", allocationSize = 1)
public class ApplicationConfiguration implements Serializable {

    private static final long serialVersionUID = 678209368462007952L;

    // attributes ////////////////////////////////////////////////////////////////////////////

    /**
     * Id of this object
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_configuration_sequence")
    private Integer id;

    @Column(name = "variable", unique = true, nullable = false)
    private String variable;

    @Column(name = "value")
    private String value;

    // constructors ////////////////////////////////////////////////////////////////////////////

    /**
     * <p>Constructor for ApplicationConfiguration.</p>
     */
    public ApplicationConfiguration() {
    }

    /**
     * <p>Constructor for ApplicationConfiguration.</p>
     *
     * @param variable a {@link java.lang.String} object.
     * @param value    a {@link java.lang.String} object.
     */
    public ApplicationConfiguration(String variable, String value) {
        this.variable = variable;
        this.value = value;
    }

    // getters and setters //////////////////////////////////////////////////////////////////////

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>variable</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getVariable() {
        return this.variable;
    }

    /**
     * <p>Setter for the field <code>variable</code>.</p>
     *
     * @param variable a {@link java.lang.String} object.
     */
    public void setVariable(String variable) {
        this.variable = variable;
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValue() {
        return this.value;
    }

    /**
     * <p>Setter for the field <code>value</code>.</p>
     *
     * @param value a {@link java.lang.String} object.
     */
    public void setValue(String value) {
        this.value = value;
    }

    // hashcode and equals //////////////////////////////////////////////////////////////////////

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.value == null) ? 0 : this.value.hashCode());
        result = (prime * result) + ((this.variable == null) ? 0 : this.variable.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        ApplicationConfiguration other = (ApplicationConfiguration) obj;
        if (this.value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!this.value.equals(other.value)) {
            return false;
        }
        if (this.variable == null) {
            if (other.variable != null) {
                return false;
            }
        } else if (!this.variable.equals(other.variable)) {
            return false;
        }
        return true;
    }

}
