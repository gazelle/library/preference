# Gazelle Preferences

Applications Preferences are stored in databases as key-values pairs. The schema **may differs from various applications**. So This module is used by Gazelle web application to abstract the different implementations. 

## How to

###Compilation

```bash
mvn clean install
```


###Usage

To be able to read or write preferences programmatically, add preferences-ejb to your pom:
```xml
<dependency>
    <groupId>net.ihe.gazelle.library</groupId>
    <artifactId>preference-ejb</artifactId>
    <version>${gazelle.preferences.version}</version>
    <type>ejb</type>
</dependency>
```

To be able to manage preferences from the GUI as administrator, add preferences-war to our pom:
```xml
<dependency>
    <groupId>net.ihe.gazelle.library</groupId>
    <artifactId>preference-war</artifactId>
    <version>${gazelle.preferences.version}</version>
    <type>ejb</type>
</dependency>
```

#### Using PreferenceProvider interface

__TO REVIEW, MAYBE WRONG__

Implement in your application the net.ihe.gazelle.preferences.PreferenceProvider interface
```java
@MetaInfServices(PreferenceProvider.class)
public class myApplicationPreferenceProvider implements PreferenceProvider {
    ...
}
```

Call `PreferenceService.getString("application_url")` to retrieve a preference value.

#### Using ApplicationConfiguration Entity

gazelle-preferences provides the ApplicationConfiguration entity composed of 2 attributes 'variable' and 'value'. It also provides its associated DAO ApplicationProviderDAO (and Implementation).

Add in the ejb, to the `META-INF/persistence.xml`

```xml
<jar-file>gazelle-preferences.jar</jar-file>
```

(gazelle-preferences.jar must be added as ejbModule in the ear)

Add in your war to `WEB-INF/components.xml`

```xml
<component class="net.ihe.gazelle.preferences.ApplicationConfigurationDAOImpl"
               jndi-name="java:module/gazelle-preferences/ApplicationConfigurationDAOImpl!net.ihe.gazelle.preferences.ApplicationConfigurationDAO"/>
```
