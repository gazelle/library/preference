package net.ihe.gazelle.preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SimplePreferences {

    private static Properties preferences;

    private static Logger log = LoggerFactory.getLogger(SimplePreferences.class);

    private SimplePreferences() {
        super();
        // private!
    }

    public static String getProperty(String key) {
        return getPreferences().getProperty(key);
    }

    private static Properties getPreferences() {
        if (preferences == null) {
            synchronized (SimplePreferences.class) {
                if (preferences == null) {
                    preferences = new Properties();
                    InputStream preferencesStream = SimplePreferences.class.getResourceAsStream("/prefs.properties");
                    try {
                        preferences.load(preferencesStream);
                        preferencesStream.close();
                    } catch (IOException e) {
                        log.error("ERROR", e);
                    }
                }
            }
        }
        return preferences;
    }
}
