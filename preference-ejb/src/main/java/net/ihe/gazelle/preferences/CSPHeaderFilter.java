package net.ihe.gazelle.preferences;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 */
@ApplicationScoped
@Named("net.ihe.gazelle.common.servletfilter.CSPHeader")
public class CSPHeaderFilter implements Filter {

    /**
     * Field httpSecurityPolicies.
     */
    private static Map<String, String> httpSecurityPolicies;

    private static Boolean httpSecurityPoliciesEnable;

    @Inject
    private CSPPoliciesPreferences cspPoliciesPreferences;

    /**
     * Method clearCache
     * Clear cached policies
     */
    public static void clearCache() {
        httpSecurityPolicies = null;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException{
    }

    @Override
    public void destroy(){}

    /**
     * Method doFilter.
     * Add http security headers to .seam content sent to the client
     *
     * @param request  ServletRequest
     * @param response ServletResponse
     * @param chain    FilterChain
     *
     * @throws IOException
     * @throws ServletException
     * @see javax.servlet.Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        if (response instanceof HttpServletResponse) {
            final HttpServletRequest httpServletRequest = (HttpServletRequest) request;

            if (isThisASeamPageRequested(httpServletRequest)) {
                applyHttpHeaderPolicy(response, httpServletRequest);
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * Method applyHttpHeaderPolicy.
     *
     * @param response           ServletResponse
     * @param httpServletRequest HttpServletRequest
     */
    private void applyHttpHeaderPolicy(ServletResponse response, final HttpServletRequest httpServletRequest) {
        loadHttpHeaderPolicy();
        if (isSecurityPolicyEnabled()) {

            final HttpServletResponse httpServletResponse = (HttpServletResponse) response;

            addHttpHeaderTo(httpServletResponse);
        }
    }

    /**
     * Method loadHttpHeaderPolicy.
     */
    private void loadHttpHeaderPolicy() {
        if (httpSecurityPolicies == null) {
            loadHttpPoliciesFromCache();
        }
    }

    private void httpSecurityPoliciesEnable() {
        if (httpSecurityPoliciesEnable == null) {
            loadHttpSecurityPoliciesEnable();
        }
    }

    /**
     * Method addHttpHeaderTo.
     *
     * @param httpServletResponse HttpServletResponse
     */
    private void addHttpHeaderTo(final HttpServletResponse httpServletResponse) {
        if (isSecurityPolicyEnabled()) {
            for (Map.Entry<String, String> httpSecurityPolicy : httpSecurityPolicies.entrySet()) {
                httpServletResponse.setHeader(httpSecurityPolicy.getKey(), httpSecurityPolicy.getValue());
            }
        }
    }

    /**
     * Method isSecurityPolicyEnabled.
     *
     * @return boolean
     */
    private boolean isSecurityPolicyEnabled() {
        if (httpSecurityPoliciesEnable == null) {
            return false;
        } else {
            return httpSecurityPoliciesEnable;
        }
    }

    /**
     * Method isThisASeamPageRequested.
     *
     * @param httpServletRequest HttpServletRequest
     *
     * @return boolean
     */
    private boolean isThisASeamPageRequested(final HttpServletRequest httpServletRequest) {
        return httpServletRequest.getRequestURI().endsWith(".seam");
    }

    /**
     * Method loadHttpPolicies.
     */
    private void loadHttpPoliciesFromCache() {
        Map<String, String> httpSecurityPoliciesFromDb = null;
        if (cspPoliciesPreferences == null){
            cspPoliciesPreferences = CDI.current().select(CSPPoliciesPreferences.class).get();
        }
        if (cspPoliciesPreferences != null) {
            httpSecurityPoliciesFromDb = cspPoliciesPreferences.getHttpSecurityPolicies();
            httpSecurityPoliciesEnable = cspPoliciesPreferences.isContentPolicyActivated();
        }
        if (httpSecurityPoliciesFromDb == null) {
            httpSecurityPoliciesFromDb = setDefaultSecurityPolicies();
            httpSecurityPoliciesEnable = false;
        }
        httpSecurityPolicies = httpSecurityPoliciesFromDb;
    }

    private void loadHttpSecurityPoliciesEnable() {

    }

    private static Map<String, String> setDefaultSecurityPolicies() {
        final Map<String, String> defaultHttpSecurityPolicies = new HashMap<String, String>();
        defaultHttpSecurityPolicies.put(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(PreferencesKey.CACHE_CONTROL.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(
                PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_CHROME.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(
                PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY_CHROME.getFriendlyName(), "");
        return defaultHttpSecurityPolicies;
    }
}