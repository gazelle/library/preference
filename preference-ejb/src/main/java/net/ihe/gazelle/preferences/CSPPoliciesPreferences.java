package net.ihe.gazelle.preferences;

import java.util.Map;

public interface CSPPoliciesPreferences {

	boolean isContentPolicyActivated();

	Map<String, String> getHttpSecurityPolicies();

	boolean getSqlInjectionFilterSwitch();
}
