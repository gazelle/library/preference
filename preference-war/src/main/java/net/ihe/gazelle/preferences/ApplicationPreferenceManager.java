package net.ihe.gazelle.preferences;

import net.ihe.gazelle.filter.Filter;
import net.ihe.gazelle.filter.FilterDataModel;
import net.ihe.gazelle.preferences.exception.EntityConstraintException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

/**
 * <p>ApplicationPreferenceManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@ManagedBean(name = "applicationPreferenceManager")
@ViewScoped
public class ApplicationPreferenceManager implements Serializable {

	@PersistenceContext
	private EntityManager entityManager;

	@Inject
	private GenericConfigurationManager applicationConfigurationManager;

	@Inject
	private ApplicationConfigurationDAO applicationConfigurationDAO;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private static Logger log = LoggerFactory.getLogger(ApplicationPreferenceManager.class);
	
	private ApplicationConfiguration preference = null;
    private Filter<ApplicationConfiguration> filter;

    private Filter<ApplicationConfiguration> getFilter(){
        if (filter == null){
            ApplicationConfigurationQuery query = new ApplicationConfigurationQuery(entityManager);
            filter = new Filter<ApplicationConfiguration>(query.getHQLCriterionsForFilter());
        }
        return filter;
    }

	/**
	 * <p>getAllPreferences.</p>
	 *
	 * @return a {@link net.ihe.gazelle.filter.FilterDataModel} object.
	 */
	public FilterDataModel<ApplicationConfiguration> getAllPreferences()
	{
        return new FilterDataModel<ApplicationConfiguration>(getFilter()) {
            @Override
            protected Object getId(ApplicationConfiguration applicationConfiguration) {
                return applicationConfiguration.getId();
            }
        };
	}
	
	/**
	 * <p>savePreference.</p>
	 *
	 * @param inPreference a {@link net.ihe.gazelle.preferences.ApplicationConfiguration} object.
	 */
	public void savePreference(ApplicationConfiguration inPreference) throws EntityConstraintException {
		applicationConfigurationDAO.save(inPreference);
		preference = null;

		FacesContext facesContext = FacesContext.getCurrentInstance();
		FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO,"Preference saved. Preferences cache reset.", null);
		facesContext.addMessage(null, facesMessage);
		// reset preferences with scope application
		applicationConfigurationManager.resetApplicationConfiguration();
	}
	
	/**
	 * <p>createNewPreference.</p>
	 */
	public void createNewPreference() {
		preference = new ApplicationConfiguration();
	}

	/**
	 * <p>editPreference.</p>
	 *
	 * @param pref a {@link net.ihe.gazelle.preferences.ApplicationConfiguration} object.
	 */
	public void editPreference(ApplicationConfiguration pref){
		preference = pref;
	}

	/**
	 * <p>Setter for the field <code>preference</code>.</p>
	 *
	 * @param preference a {@link net.ihe.gazelle.preferences.ApplicationConfiguration} object.
	 */
	public void setPreference(ApplicationConfiguration preference) {
		this.preference = preference;
	}

	/**
	 * <p>Getter for the field <code>preference</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.preferences.ApplicationConfiguration} object.
	 */
	public ApplicationConfiguration getPreference() {
		return preference;
	}

	/**
	 * <p>resetHttpHeaders.</p>
	 */
	public void resetHttpHeaders() {
		log.info("Reset http headers to default values");
        applicationConfigurationManager.resetHttpHeaders();
	}
	
	/**
	 * <p>updateHttpHeaders.</p>
	 */
	public void updateHttpHeaders(){
		CSPHeaderFilter.clearCache();
	}
	
}
