/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.preferences;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import javax.ejb.Remove;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.Serializable;

/**
 *  <b>Class Description :  </b>GenericConfigurationManager<br><br>
 * This class contains the methods which are called in the footer modal panels
 *
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version                	1.0 - 2010, October 15th
 */

@Named("genericConfigurationManager")
@ApplicationScoped
public class GenericConfigurationManager implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7817765881624341837L;
	private static Logger log = LoggerFactory.getLogger(GenericConfigurationManager.class);
	
	private String releaseNoteUrl;
	private String documentation;
	private Boolean worksWithoutCas;
	private String applicationUrl;
	private String casUrl;
	private Boolean ipLogin;
	private String ipLoginAdmin;
	private String contactTitle;
	private String contactEmail;
	private String contactName;
	private String issueTracker;

	@PersistenceContext
	private EntityManager entityManager;

	@Inject
	private ApplicationConfigurationDAO applicationConfigurationDAO;

	/**
	 * <p>destroy.</p>
	 */
	@Remove
	@PreDestroy
	public void destroy() {

	}

	/**
	 * <p>resetApplicationConfiguration.</p>
	 */
	public void resetApplicationConfiguration() {
		releaseNoteUrl = null;
		documentation = null;
		worksWithoutCas = null;
		casUrl = null;
		applicationUrl = null;
		ipLogin = null;
		ipLoginAdmin = null;
		contactTitle = null;
		contactEmail = null;
		contactName = null;
		issueTracker = null;
	}
	

	/**
	 * <p>Getter for the field <code>releaseNoteUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getReleaseNoteUrl() {
		if (releaseNoteUrl == null) {
			releaseNoteUrl = applicationConfigurationDAO.getValue("application_release_notes_url");
			if (releaseNoteUrl == null) {
				log.error("application_release_notes_url variable is missing in app_configuration table");
			}
		}
		return releaseNoteUrl;
	}

	/**
	 * <p>Getter for the field <code>documentation</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDocumentation(){
	    if (this.documentation == null){
	        this.documentation = applicationConfigurationDAO.getValue("documentation_url");
	    }
	    if (this.documentation == null){
	    	log.error("application_documentation variable is missing in app_configuration table");
	        this.documentation = "http://gazelle.ihe.net";
	    }
	    return this.documentation;
	}

	/**
	 * <p>isWorksWithoutCas.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isWorksWithoutCas() {
		if (worksWithoutCas == null) {
			String booleanAsString = applicationConfigurationDAO.getValue("application_works_without_cas");
			if (booleanAsString == null) {
				this.worksWithoutCas = false;
			} else {
				this.worksWithoutCas = Boolean.valueOf(booleanAsString);
			}
		}
		return worksWithoutCas;
	}
	
	/**
	 * <p>Getter for the field <code>applicationUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getApplicationUrl() {
		if (applicationUrl == null){
			this.applicationUrl = applicationConfigurationDAO.getValue("application_url");
		}
		return applicationUrl;
	}

	/**
	 * <p>Getter for the field <code>casUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCasUrl() {
		if (this.casUrl == null){
			this.casUrl = applicationConfigurationDAO.getValue("cas_url");
		}
		return casUrl;
	}

	/**
	 * <p>Getter for the field <code>ipLogin</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getIpLogin() {
		if (ipLogin == null)
		{
			String booleanAsString = applicationConfigurationDAO.getValue("ip_login");
			if (booleanAsString == null)
			{
				this.ipLogin = false;
			}
			else
			{
				this.ipLogin = Boolean.valueOf(booleanAsString);
			}
		}
		return ipLogin;
	}

	/**
	 * <p>Getter for the field <code>ipLoginAdmin</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIpLoginAdmin() {
		if (this.ipLoginAdmin == null){
			this.ipLoginAdmin = applicationConfigurationDAO.getValue("ip_login_admin");
		}
		return ipLoginAdmin;
	}

	/**
	 * <p>Getter for the field <code>contactTitle</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getContactTitle(){
		if (this.contactTitle == null) {
			this.contactTitle = applicationConfigurationDAO.getValue("contact_title");
		}
		return this.contactTitle;
	}

	/**
	 * <p>Getter for the field <code>contactEmail</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getContactEmail(){
		if (this.contactEmail == null) {
			this.contactEmail = applicationConfigurationDAO.getValue("contact_email");
		}
		return this.contactEmail;
	}

	/**
	 * <p>Getter for the field <code>contactName</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getContactName(){
		if (this.contactName == null) {
			this.contactName = applicationConfigurationDAO.getValue("contact_name");
		}
		return this.contactName;
	}

	/**
	 * <p>Getter for the field <code>issueTracker</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIssueTracker(){
		if (this.issueTracker == null) {
			this.issueTracker = applicationConfigurationDAO.getValue("application_issue_tracker");
		}
		return this.issueTracker;
	}

	/**
	 * <p>getValueOfVariable.</p>
	 *
	 * @param variable a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	@Deprecated
	//Use DAO Instead
	public String getValueOfVariable(String variable) {
		return getValueOfVariable(variable, entityManager);
	}

	/**
	 * <p>getValueOfVariable.</p>
	 *
	 * @param variable      a {@link java.lang.String} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link java.lang.String} object.
	 */
	@Deprecated
	//Use DAO Instead
	public String getValueOfVariable(String variable, EntityManager entityManager) {
		ApplicationConfigurationQuery query = new ApplicationConfigurationQuery(
				new HQLQueryBuilder<ApplicationConfiguration>(entityManager, ApplicationConfiguration.class));
		query.variable().eq(variable);
		ApplicationConfiguration conf = query.getUniqueResult();
		if (conf == null) {
			return null;
		} else {
			return conf.getValue();
		}
	}

	/**
	 * <p>resetHttpHeaders.</p>
	 */
	@Transactional
	public void resetHttpHeaders() {
		ApplicationConfiguration ap;

		for (PreferencesKey preference : PreferencesKey.values()) {
			ApplicationConfigurationQuery applicationConfigurationQuery = new ApplicationConfigurationQuery();
			String friendlyName = preference.getFriendlyName();
			applicationConfigurationQuery.variable().eq(friendlyName);
			ap = applicationConfigurationQuery.getUniqueResult();
			if (ap == null) {
				ap = new ApplicationConfiguration();
			}
			ap.setVariable(friendlyName);
			ap.setValue(preference.getDefaultValue());
			entityManager.merge(ap);
			entityManager.flush();
		}
		CSPHeaderFilter.clearCache();
	}

	@Deprecated
	//Use DAO Instead
	public Boolean getBooleanValue(String variable) {
		String val = getValueOfVariable(variable);
		if (val == null) {
			return false;
		}
		try {
			return Boolean.valueOf(val);
		} catch (Exception e) {
			return false;
		}
	}

	@Transactional
	@Deprecated
	//Use DAO Instead
	public ApplicationConfiguration saveApplicationConfiguration(ApplicationConfiguration applicationConfiguration){
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		ApplicationConfiguration mergedApplicationConfiguration = entityManager.merge(applicationConfiguration);
		entityManager.flush();
		return mergedApplicationConfiguration;
	}

	@Deprecated
	//Use SSO instead
	public String loginByIP() {
		return getApplicationUrl() + "/home.xhtml";
	}
}
